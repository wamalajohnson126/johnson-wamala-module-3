import 'package:flutter/material.dart';

// ignore: camel_case_types
class forthPage extends StatefulWidget {
  const forthPage({ Key? key }) : super(key: key);

  @override
  State<forthPage> createState() => _forthPageState();
}

// ignore: camel_case_types
class _forthPageState extends State<forthPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
       title: const Text("Feature Page"),
     ),
     body: Center(
       child: ElevatedButton(
         child: const Text("Go back to home page"),
         onPressed: () {
           Navigator.pop(context);
         },
          ),
     ),
    );
  }
}

