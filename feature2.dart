import 'package:flutter/material.dart';

// ignore: camel_case_types
class fifthPage extends StatefulWidget {
  const fifthPage({ Key? key }) : super(key: key);

  @override
  State<fifthPage> createState() => _fifthPageState();
}

// ignore: camel_case_types
class _fifthPageState extends State<fifthPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
       title: const Text("New Feature Page"),
     ),
     body: Center(
       child: ElevatedButton(
         child: const Text("Go back to home page"),
         onPressed: () {
           Navigator.pop(context);
         },
          ),
     ),
    );
  }
}