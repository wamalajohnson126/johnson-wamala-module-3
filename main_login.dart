import 'package:flutter/material.dart';
import 'package:flutter_module_q3/screens/dashboard.dart';
import 'package:flutter_module_q3/screens/register.dart';
// ignore: camel_case_types
class mainPage extends StatefulWidget {
  const mainPage({ Key? key }) : super(key: key);

  @override
  State<mainPage> createState() => _mainPageState();
}

// ignore: camel_case_types
class _mainPageState extends State<mainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
       title: const Text("Login Page"),
     ),   
     body: Center(      
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          children: <Widget> [
            Column(
              // ignore: prefer_const_literals_to_create_immutables
              children:<Widget> [
                const SizedBox(height: 120,),
                const SizedBox(height: 30,),
                const Text("User Login" ,style: TextStyle(fontSize: 25,color: Colors.blue),)
              ],
            ),
             const SizedBox(height: 60.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "User Name:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.blue),
                filled: true,
               )
            ),
            const SizedBox(height: 20.0,),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: "Password:",
                labelStyle: TextStyle(fontSize: 20.0,color: Colors.blue),
                filled: true,
              ),
            ),
            const SizedBox(height: 20.0,),
          Column(
            children: <Widget> [
              ButtonTheme(height: 80.0,
               disabledColor: Colors.greenAccent,
                child: InkWell(
            child: const Text("Login",style: TextStyle(fontSize: 25.0,color: Colors.green),),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => const secondPage() ));
            },
          )
               ),
               const SizedBox(height: 20.0,),
               InkWell(
            child: const Text("New User? Register Here",style: TextStyle(fontSize: 15.0,color: Colors.blue),),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => const thirdPage() ));
            },
          )
            ],
          ),
          ],
        ),
     ),
    );
  }
}